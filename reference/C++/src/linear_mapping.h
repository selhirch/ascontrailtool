/*
AsconTrailTool, a set of C++ classes for analyzing ASCON.

For specifications, please refer to https://tosc.iacr.org/index.php/ToSC/article/view/9975

Implementation by Solane El Hirch, hereby denoted as "the implementer".
*/

#ifndef LINEARMAPPING_H
#define LINEARMAPPING_H

uint64_t shift64(uint64_t image, int x);

#endif /* LINEARMAPPING_H */
